#!/usr/bin/env python3

################################################################################
# SPDX-FileCopyrightText: Copyright (c) 2019-2021 NVIDIA CORPORATION & AFFILIATES. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

import sys
sys.path.append('../')
import platform
import configparser

import gi
gi.require_version('Gst', '1.0')
from gi.repository import GObject, Gst
from common.is_aarch_64 import is_aarch64
from common.bus_call import bus_call
from common.FPS import GETFPS

import pyds
import ctypes
import numpy as np
import yaml
import cv2
from skimage import transform as trans
import math

fps_streams = {}


OSD_PROCESS_MODE= 1
OSD_DISPLAY_TEXT= 1
TILED_OUTPUT_WIDTH=1080
TILED_OUTPUT_HEIGHT=720
GST_CAPS_FEATURES_NVMM="memory:NVMM"

PGIE_CLASS_ID_FACE = 0

past_tracking_meta=[0]

def facePoints2(image, faceLandmarks, color=(0, 255, 0), radius=4):
	for p in faceLandmarks:
		cv2.circle(image, (int(p[0]), int(p[1])), radius, color, -1)
	return image

def osd_sink_pad_buffer_probe(pad,info,u_data):
	frame_number=0
	#Intiallizing object counter with 0.
	obj_counter = {
		PGIE_CLASS_ID_FACE:0,
	}
	num_rects=0
	gst_buffer = info.get_buffer()
	if not gst_buffer:
		print("Unable to get GstBuffer ")
		return

	# Retrieve batch metadata from the gst_buffer
	# Note that pyds.gst_buffer_get_nvds_batch_meta() expects the
	# C address of gst_buffer as input, which is obtained with hash(gst_buffer)
	batch_meta = pyds.gst_buffer_get_nvds_batch_meta(hash(gst_buffer))
	l_frame = batch_meta.frame_meta_list
	while l_frame is not None:
		try:
			# Note that l_frame.data needs a cast to pyds.NvDsFrameMeta
			# The casting is done by pyds.NvDsFrameMeta.cast()
			# The casting also keeps ownership of the underlying memory
			# in the C code, so the Python garbage collector will leave
			# it alone.
			frame_meta = pyds.NvDsFrameMeta.cast(l_frame.data)
		except StopIteration:
			break

		frame_number=frame_meta.frame_num
		num_rects = frame_meta.num_obj_meta
		l_obj=frame_meta.obj_meta_list
		while l_obj is not None:
			n_frame=pyds.get_nvds_buf_surface(hash(gst_buffer),frame_meta.batch_id)
			frame_image=np.array(n_frame,copy=True,order='C')
			frame_image=cv2.cvtColor(frame_image,cv2.COLOR_RGBA2BGRA)
			frame_image = cv2.cvtColor(frame_image, cv2.COLOR_BGRA2BGR)
				
			try:
				# Casting l_obj.data to pyds.NvDsObjectMeta
				obj_meta=pyds.NvDsObjectMeta.cast(l_obj.data)
			except StopIteration:
				break
			obj_counter[obj_meta.class_id] += 1

			###################################

			l_user = obj_meta.obj_user_meta_list

			if obj_meta.class_id == PGIE_CLASS_ID_FACE:
				left = int(obj_meta.rect_params.left)
				top = int(obj_meta.rect_params.top)
				width = int(obj_meta.rect_params.width)
				height = int(obj_meta.rect_params.height)

			while l_user is not None:
			   
				try:
					# Casting l_user.data to pyds.NvDsUserMeta
					user_meta=pyds.NvDsUserMeta.cast(l_user.data)
				except Exception as e:
					print(e)
					break
				# We can only access NVDSINFER_TENSOR_OUTPUT_META of Facial Landmark model since SET: output-tensor-meta=1
				
				if (
					user_meta.base_meta.meta_type
					!= pyds.NvDsMetaType.NVDSINFER_TENSOR_OUTPUT_META
				):
					continue
				
				# Converting to tensor metadata
				# Casting user_meta.user_meta_data to NvDsInferTensorMeta
				tensor_meta = pyds.NvDsInferTensorMeta.cast(user_meta.user_meta_data)
				print(tensor_meta)
				layers_info = []
				for i in range(tensor_meta.num_output_layers):
					layer = pyds.get_nvds_LayerInfo(tensor_meta, i)
					# Convert NvDsInferLayerInfo buffer to numpy array
					ptr = ctypes.cast(pyds.get_ptr(layer.buffer), ctypes.POINTER(ctypes.c_float))
					if "softargmax/strided_slice:0" in layer.layerName:
						# Output: 80,2
						heatmap_data = np.ctypeslib.as_array(ptr, shape=(80,2))
						# print(heatmap_data)
						# Extract Lanmark point the same as MTCNN: 
						# Left eye:  , Right eye: , left mouth: 49 right mouth: 55 , nose: 34
						# Point dlib: https://www.studytonight.com/post/dlib-68-points-face-landmark-detection-with-opencv-and-python
						crop_image = frame_image[top:top + height, left:left + width, :]
						# From 69-72: Eye: left, top, right, down
						left_eye = [(heatmap_data[68][0] + heatmap_data[70][0]) /2 , (heatmap_data[69][1] + heatmap_data[71][1]) /2]
						right_eye = [(heatmap_data[72][0] + heatmap_data[74][0]) /2 , (heatmap_data[73][1] + heatmap_data[75][1]) /2]
						left_mouth = heatmap_data[48]
						right_mouth = heatmap_data[54]
						nose = heatmap_data[33]

						points = np.array([left_eye, right_eye, nose, left_mouth, right_mouth])
						# Rescale points to origin coordinate in Face Box
						intputLandmarkSize = 80
						points[:,0] = points[:,0]*width/intputLandmarkSize
						points[:,1] = points[:,1]*height/intputLandmarkSize
						# Transform Face with 5 standard landmarks
						fixed_landmarks = np.array(
							[[38.2946, 51.6963], [73.5318, 51.5014], [56.0252, 71.7366], [41.5493, 92.3655],
							[70.7299, 92.2041]], dtype=np.float32)
            
						tform = trans.SimilarityTransform()
						tform.estimate(points, fixed_landmarks)
						M = tform.params[0:2, :]
						image_size = 112
						crop_image = frame_image[top:top + height, left:left + width, :]
						aligned = cv2.warpAffine(crop_image, M, (image_size, image_size), borderValue=0.0)
						result = facePoints2(aligned, fixed_landmarks,color=(0, 255, 0), radius=3)
						cv2.imwrite('FRAME/' + str(frame_number) + '.jpg', result)
					elif "softargmax/strided_slice_1:0" in layer.layerName:
						# Output: 80,
						confidence = np.ctypeslib.as_array(ptr, shape=(80,))
				
				try:
					l_user = l_user.next
				except Exception as e:
					print(e)
					break

			try: 
				l_obj=l_obj.next
			except StopIteration:
				break

		# Acquiring a display meta object. The memory ownership remains in
		# the C code so downstream plugins can still access it. Otherwise
		# the garbage collector will claim it when this probe function exits.
		display_meta=pyds.nvds_acquire_display_meta_from_pool(batch_meta)
		display_meta.num_labels = 1
		py_nvosd_text_params = display_meta.text_params[0]
		# Setting display text to be shown on screen
		# Note that the pyds module allocates a buffer for the string, and the
		# memory will not be claimed by the garbage collector.
		# Reading the display_text field here will return the C address of the
		# allocated string. Use pyds.get_string() to get the string content.
		py_nvosd_text_params.display_text = "Frame Number={} Number of Objects={} FACE={}".format(frame_number, num_rects, obj_counter[PGIE_CLASS_ID_FACE])

		# Now set the offsets where the string should appear
		py_nvosd_text_params.x_offset = 10
		py_nvosd_text_params.y_offset = 12

		# Font , font-color and font-size
		py_nvosd_text_params.font_params.font_name = "Serif"
		py_nvosd_text_params.font_params.font_size = 10
		# set(red, green, blue, alpha); set to White
		py_nvosd_text_params.font_params.font_color.set(1.0, 1.0, 1.0, 1.0)

		# Text background color
		py_nvosd_text_params.set_bg_clr = 1
		# set(red, green, blue, alpha); set to Black
		py_nvosd_text_params.text_bg_clr.set(0.0, 0.0, 0.0, 1.0)
		# Using pyds.get_string() to get display_text as string
		print(pyds.get_string(py_nvosd_text_params.display_text))
		pyds.nvds_add_display_meta_to_frame(frame_meta, display_meta)
		try:
			l_frame=l_frame.next
		except StopIteration:
			break
	return Gst.PadProbeReturn.OK	

def cb_newpad(decodebin, decoder_src_pad,data):
	print("In cb_newpad\n")
	caps=decoder_src_pad.get_current_caps()
	gststruct=caps.get_structure(0)
	gstname=gststruct.get_name()
	source_bin=data
	features=caps.get_features(0)

	# Need to check if the pad created by the decodebin is for video and not
	# audio.
	print("gstname={}".format(gstname))
	if(gstname.find("video")!=-1):
		# Link the decodebin pad only if decodebin has picked nvidia
		# decoder plugin nvdec_*. We do this by checking if the pad caps contain
		# NVMM memory features.
		print("features={}".format(features))
		if features.contains("memory:NVMM"):
			# Get the source bin ghost pad
			bin_ghost_pad=source_bin.get_static_pad("src")
			if not bin_ghost_pad.set_target(decoder_src_pad):
				sys.stderr.write("Failed to link decoder src pad to source bin ghost pad\n")
		else:
			sys.stderr.write(" Error: Decodebin did not pick nvidia decoder plugin.\n")

def decodebin_child_added(child_proxy,Object,name,user_data):
	print("Decodebin child added:{}".format(name))
	if(name.find("decodebin") != -1):
		Object.connect("child-added",decodebin_child_added,user_data)

def create_source_bin(index,uri):
	print("Creating source bin")

	# Create a source GstBin to abstract this bin's content from the rest of the
	# pipeline
	bin_name="source-bin-%02d" %index
	print(bin_name)
	nbin=Gst.Bin.new(bin_name)
	if not nbin:
		sys.stderr.write(" Unable to create source bin \n")

	# Source element for reading from the uri.
	# We will use decodebin and let it figure out the container format of the
	# stream and the codec and plug the appropriate demux and decode plugins.
	uri_decode_bin=Gst.ElementFactory.make("uridecodebin", "uri-decode-bin")
	if not uri_decode_bin:
		sys.stderr.write(" Unable to create uri decode bin \n")
	# We set the input uri to the source element
	uri_decode_bin.set_property("uri",uri)
	# Connect to the "pad-added" signal of the decodebin which generates a
	# callback once a new pad for raw data has beed created by the decodebin
	uri_decode_bin.connect("pad-added",cb_newpad,nbin)
	uri_decode_bin.connect("child-added",decodebin_child_added,nbin)

	# We need to create a ghost pad for the source bin which will act as a proxy
	# for the video decoder src pad. The ghost pad will not have a target right
	# now. Once the decode bin creates the video decoder and generates the
	# cb_newpad callback, we will set the ghost pad target to the video decoder
	# src pad.
	Gst.Bin.add(nbin,uri_decode_bin)
	bin_pad=nbin.add_pad(Gst.GhostPad.new_no_target("src",Gst.PadDirection.SRC))
	if not bin_pad:
		sys.stderr.write(" Failed to add ghost pad in source bin \n")
		return None
	return nbin

def main():
    # Load config
	with open("config/config_perception.yml", "r") as stream:
		try:
			dataConfig = yaml.safe_load(stream)
		except yaml.YAMLError as exc:
			print(exc)
	# Start Camera object

	isVisualize = dataConfig['isVisualize'] # For Debug

	number_sources = len(dataConfig['listCamera'])

	# Standard GStreamer initialization
	GObject.threads_init()
	Gst.init(None)

	# Create gstreamer elements
	# Create Pipeline element that will form a connection of other elements
	print("Creating Pipeline \n ")
	pipeline = Gst.Pipeline()
	is_live = False

	if not pipeline:
		sys.stderr.write(" Unable to create Pipeline \n")
    
	print("Creating streamux \n ")

	# Create nvstreammux instance to form batches from one or more sources.
	streammux = Gst.ElementFactory.make("nvstreammux", "Stream-muxer")
	if not streammux:
		sys.stderr.write(" Unable to create NvStreamMux \n")

	pipeline.add(streammux)
	for i in range(number_sources):
		print("Creating source_bin %d"%i)
		uri_name = dataConfig['listCamera'][i]
		if uri_name.find("rtsp://") == 0 :
			is_live = True
		source_bin=create_source_bin(i, uri_name)
		if not source_bin:
			sys.stderr.write("Unable to create source bin \n")
		pipeline.add(source_bin)
		padname="sink_%u" %i
		sinkpad= streammux.get_request_pad(padname) 
		if not sinkpad:
			sys.stderr.write("Unable to create sink pad bin \n")
		srcpad=source_bin.get_static_pad("src")
		if not srcpad:
			sys.stderr.write("Unable to create src pad bin \n")
		srcpad.link(sinkpad)
   
	queue1=Gst.ElementFactory.make("queue","queue1")
	queue2=Gst.ElementFactory.make("queue","queue2")
	queue3=Gst.ElementFactory.make("queue","queue3")
	queue4=Gst.ElementFactory.make("queue","queue4")
	queue5=Gst.ElementFactory.make("queue","queue5")
	queue6=Gst.ElementFactory.make("queue","queue6")
	queue7=Gst.ElementFactory.make("queue","queue7")
	pipeline.add(queue1)
	pipeline.add(queue2)
	pipeline.add(queue3)
	pipeline.add(queue4)
	pipeline.add(queue5)
	pipeline.add(queue6)
	pipeline.add(queue7)

	# Use nvinfer to run inferencing on decoder's output,
	# behaviour of inferencing is set through config file
	pgie = Gst.ElementFactory.make("nvinfer", "primary-inference face detector")
	if not pgie:
		sys.stderr.write(" Unable to create pgie \n")

	sgie = Gst.ElementFactory.make("nvinfer", "second-inference")
	if not sgie:
		sys.stderr.write(" Unable to create sgie \n")

	sgie2 = Gst.ElementFactory.make("nvinfer", "third-inference")
	if not sgie2:
		sys.stderr.write(" Unable to create sgie2 \n")

	print("Creating tiler \n ")
	#When using nvmultistreamtiler plugin in your pipeline, the multiple streams are merged to one stream
	# See more: https://forums.developer.nvidia.com/t/differentiate-source-of-frame-for-multiple-sources-application/144378
	tiler=Gst.ElementFactory.make("nvmultistreamtiler", "nvtiler")
	if not tiler:
		sys.stderr.write(" Unable to create tiler \n")


	# Use convertor to convert from NV12 to RGBA as required by nvosd
	nvvidconv = Gst.ElementFactory.make("nvvideoconvert", "convertor")
	if not nvvidconv:
		sys.stderr.write(" Unable to create nvvidconv \n")

	nvvidconv1 = Gst.ElementFactory.make("nvvideoconvert", "convertor1")
	if not nvvidconv1:
		sys.stderr.write(" Unable to create nvvidconv1 \n")

	print("Creating filter1 \n ")
	caps1 = Gst.Caps.from_string("video/x-raw(memory:NVMM), format=RGBA")
	filter1 = Gst.ElementFactory.make("capsfilter", "filter1")
	if not filter1:
		sys.stderr.write(" Unable to get the caps filter1 \n")
	filter1.set_property("caps", caps1)
	
	print("Creating filter2 \n ")
	caps2 = Gst.Caps.from_string("video/x-raw(memory:NVMM), format=RGBA")
	filter2 = Gst.ElementFactory.make("capsfilter", "filter2")
	if not filter2:
		sys.stderr.write(" Unable to get the caps filter2 \n")
	filter2.set_property("caps", caps2)
	
	
	# Create OSD to draw on the converted RGBA buffer
	nvosd = Gst.ElementFactory.make("nvdsosd", "onscreendisplay")

	if not nvosd:
		sys.stderr.write(" Unable to create nvosd \n")

	nvosd.set_property('process-mode',OSD_PROCESS_MODE)
	nvosd.set_property('display-text',OSD_DISPLAY_TEXT)
	# Finally render the osd output
	if is_aarch64():
		transform = Gst.ElementFactory.make("nvegltransform", "nvegl-transform")
		if not transform:
			sys.stderr.write(" Unable to create transform \n")

	print("Creating EGLSink \n")
	# For info Visualize https://forums.developer.nvidia.com/t/stop-display-for-python-deepstream-app/175748/3
	if isVisualize:
		sink = Gst.ElementFactory.make("nveglglessink", "nvvideo-renderer") # Turn on visualize
	else:
		sink = Gst.ElementFactory.make("fakesink", "nvvideo-renderer") # Turn off visualize
	if not sink:
		sys.stderr.write(" Unable to create egl sink \n")

	if is_live:
		print("Atleast one of the sources is live")
		streammux.set_property('live-source', 1)

	streammux.set_property('width', 1080)
	streammux.set_property('height', 720)
	streammux.set_property('batch-size', number_sources)
	max_fps = 20
	streammux.set_property('batched-push-timeout', 1/max_fps)
	#https://docs.nvidia.com/metropolis/deepstream/dev-guide/text/DS_troubleshooting.html
	#On Jetson in the configuration file of gst-nvinfer set scaling-compute-hw = 1 if gpu usage is not 100%.
	streammux.set_property('compute-hw', 1)
	pgie.set_property('config-file-path',"../../../configs/facial_tao/config_infer_primary_facenet.txt")
	
	pgie_batch_size = pgie.get_property("batch-size")
	if(pgie_batch_size != number_sources):
		print("WARNING: Overriding infer-config batch-size " + str(pgie_batch_size) + " with number of sources " + str(number_sources) + " \n")
		pgie.set_property("batch-size",number_sources)

	sgie.set_property('config-file-path', "../../../configs/facial_tao/faciallandmark_sgie_config.txt")
	sgie2.set_property('config-file-path', "configDeepstream/emotion_config.txt")


	tiler_rows=int(math.sqrt(number_sources))
	tiler_columns=int(math.ceil((1.0*number_sources)/tiler_rows))
	tiler.set_property("rows",tiler_rows)
	tiler.set_property("columns",tiler_columns)
	tiler.set_property("width", TILED_OUTPUT_WIDTH)
	tiler.set_property("height", TILED_OUTPUT_HEIGHT)
	sink.set_property("sync",0)


	print("Adding elements to Pipeline \n")
	pipeline.add(sgie)
	pipeline.add(pgie)
	pipeline.add(sgie2)
	pipeline.add(tiler)
	pipeline.add(filter1)
	pipeline.add(filter2)
	pipeline.add(nvvidconv)
	pipeline.add(nvvidconv1)
	pipeline.add(nvosd)
	pipeline.add(sink)
	if is_aarch64():
		pipeline.add(transform)

	# we link the elements together
	# Pineline: https://forums.developer.nvidia.com/t/deepstream-python-bindings-cannot-access-frame-from-deepstream-test-app3/153804/6
	# source_bin -> streammux --> pgie --> tracker --> nvvidconv --> \
	#  capsfilter --> tiler --> nvvidconv --> nvosd --> sink
	print("Linking elements in the Pipeline \n")
	streammux.link(queue1)
	queue1.link(pgie)
	pgie.link(queue2)
	queue2.link(sgie)
	sgie.link(queue3)
	queue3.link(sgie2)
	sgie2.link(queue7)
	queue7.link(tiler)
	tiler.link(queue4)
	
	queue4.link(nvvidconv1)
	nvvidconv1.link(queue5)
	queue5.link(nvosd)

	if is_aarch64() and isVisualize: # For Visualize Jetson
		nvosd.link(queue6)
		queue6.link(transform)
		transform.link(sink)
	else: # Turn off visualize for Jetson, also work with X86 Server
		nvosd.link(queue6)
		queue6.link(sink)

	# create an event loop and feed gstreamer bus mesages to it
	loop = GObject.MainLoop()
	bus = pipeline.get_bus()
	bus.add_signal_watch()
	bus.connect ("message", bus_call, loop)

	# Lets add probe to get informed of the meta data generated, we add probe to
	# the sink pad of the osd element, since by that time, the buffer would have
	# had got all the metadata.
	# ERROR: Currently we only support RGBA color Format error
	# Solution: https://forums.developer.nvidia.com/t/currently-we-only-support-rgba-color-format-error/166159/4
	# Because: OSD return RGBA
	osdsinkpad = nvosd.get_static_pad("sink")
	if not osdsinkpad:
		sys.stderr.write(" Unable to get sink pad of nvosd \n")

	osdsinkpad.add_probe(Gst.PadProbeType.BUFFER, osd_sink_pad_buffer_probe, 0)
	# List the sources
	print("Now playing...")
	for i, source in enumerate(dataConfig['listCamera']):
		print('%d %s'%(i, source))

	# start play back and listen to events
	print("Starting pipeline \n")
	pipeline.set_state(Gst.State.PLAYING)
	try:
		loop.run()
	except:
		pass
	# cleanup
	print("Exiting app\n")
	pipeline.set_state(Gst.State.NULL)


if __name__ == '__main__':
	sys.exit(main())
